# REFERENCE DATE COMBO FIELD

## INTRODUCTION

The Reference Date Combo field module provides an single combined field with both an entity reference sub-field and two
date fields as a new field type. See also [Reference Value Pair](https://www.drupal.org/project/reference_value_pair)
module for a similar idea.

Reduces the table/database bloat/simplifies views/queries when you need to store a date and a entity reference together
versus other standard Drupal 8 solutions such as Paragraphs or ECK.

## REQUIREMENTS

- Drupal core 9.0+ with datetime module enabled.

## INSTALLATION

- Install as you would normally install a contributed Drupal module. See also the core
  [Documentation](https://www.drupal.org/node/1897420) further information.

## CONFIGURATION

Add the newly available Reference Date combo field to any fieldable entity as desired.

## MAINTAINERS

- [Nick Dickinson-Wilde](https://www.drupal.org/u/nickdickinsonwilde)
