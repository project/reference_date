<?php

namespace Drupal\reference_date\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the 'reference_date_combo' field type.
 *
 * @FieldType(
 *   id = "reference_date_combo",
 *   label = @Translation("Reference Date Combo"),
 *   category = @Translation("General"),
 *   default_widget = "reference_date_combo",
 *   default_formatter = "reference_date_combo_default",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList"
 * )
 */
class ReferenceDateComboItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = parent::defaultStorageSettings();
    $settings['end_date'] = FALSE;
    $settings['datetime_type'] = DateTimeItem::DATETIME_TYPE_DATE;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);
    $settings = $this->getSettings();
    $element['datetime_type'] = [
      '#type' => 'select',
      '#title' => t('Date type'),
      '#description' => t('Choose the type of date to create.'),
      '#default_value' => $this->getSetting('datetime_type'),
      '#options' => [
        DateTimeItem::DATETIME_TYPE_DATETIME => t('Date and time'),
        DateTimeItem::DATETIME_TYPE_DATE => t('Date only'),
      ],
      '#disabled' => $has_data,
    ];
    $element['end_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show End Date Field'),
      '#default_value' => $settings['end_date'],
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties = parent::propertyDefinitions($field_definition);
    $properties['value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Start Date'));
    $properties['date'] = DataDefinition::create('any')
      ->setLabel(t('Computed date'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\datetime\DateTimeComputed')
      ->setSetting('date source', 'value');
    $properties['end_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('End Date'));
    $properties['end_date'] = DataDefinition::create('any')
      ->setLabel(t('Computed End date'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\datetime\DateTimeComputed')
      ->setSetting('date source', 'end_value');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = parent::schema($field_definition);
    $schema['columns']['value'] = [
      'type' => 'varchar',
      'length' => 20,
    ];
    $schema['columns']['end_value'] = [
      'type' => 'varchar',
      'length' => 20,
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $options['target_id']['NotBlank'] = [];
    $options['value']['NotBlank'] = [];

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ComplexData', $options);
    // @todo Add more constraints here?
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {

    $values['target_id'] = mt_rand(1, 1000);

    $timestamp = \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365);
    $values['value'] = gmdate('Y-m-d\TH:i:s', $timestamp);

    $timestamp = \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365);
    $values['end_value'] = gmdate('Y-m-d\TH:i:s', $timestamp);

    return $values;
  }

}
